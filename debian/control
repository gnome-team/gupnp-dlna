Source: gupnp-dlna
Section: libs
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Jeremy Bícha <jbicha@ubuntu.com>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-gir,
               libxml2-dev,
               libxml2-utils,
               libgstreamer1.0-dev (>= 1.0),
               libgstreamer-plugins-base1.0-dev (>= 1.0),
               libglib2.0-dev (>= 2.32),
               valac (>= 0.20),
               gtk-doc-tools,
               libgirepository1.0-dev (>= 0.9.12)
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/gnome-team/gupnp-dlna
Vcs-Git: https://salsa.debian.org/gnome-team/gupnp-dlna.git
Homepage: https://wiki.gnome.org/Projects/GUPnP

Package: libgupnp-dlna-2.0-4
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Breaks: libgupnp-dlna-2.0-3 (<< 0.12)
Replaces: libgupnp-dlna-2.0-3 (<< 0.12)
Description: DLNA utility library for GUPnP
 A small utility library that aims to ease the DLNA-related tasks
 such as media profile guessing, transcoding to a given profile, etc.
Multi-Arch: same

Package: gir1.2-gupnpdlna-2.0
Section: introspection
Architecture: any
Depends: ${misc:Depends}, ${gir:Depends}
Conflicts: gir1.0-gupnp-1.0, gir1.2-gupnp-dlna-1.0 (<< 0.6.6~)
Replaces: gir1.0-gupnp-1.0, gir1.2-gupnp-dlna-1.0 (<< 0.6.6~)
Provides: gir1.2-gupnpdlnagst-2.0 (= ${binary:Version})
Description: GObject introspection data for the DLNA utility library for GUPnP
 This package contains introspection data for the DLNA utility library
 for GUPnP, a UPnP library.
 .
 It can be used by packages using the GIRepository format to generate
 dynamic bindings.

Package: libgupnp-dlna-2.0-dev
Section: libdevel
Architecture: any
Depends: libgupnp-dlna-2.0-4 (= ${binary:Version}),
         gir1.2-gupnpdlna-2.0 (= ${binary:Version}),
         libgstreamer-plugins-base1.0-dev,
         ${misc:Depends}
Description: DLNA utility library for GUPnP (development files)
 A small utility library that aims to ease the DLNA-related tasks
 such as media profile guessing, transcoding to a given profile, etc.
 .
 This package contains the development files.

Package: libgupnp-dlna-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Suggests: devhelp
Description: DLNA utility library for GUPnP (documentation)
 A small utility library that aims to ease the DLNA-related tasks
 such as media profile guessing, transcoding to a given profile, etc.
 .
 This package contains the documentation.
Multi-Arch: foreign

Package: gupnp-dlna-tools
Section: net
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: GObject-based library for GUPnP DLNA (tools)
 A small utility library that aims to ease the DLNA-related tasks
 such as media profile guessing, transcoding to a given profile, etc.
 .
 This package contain tools, like gupnp-dlna-info.
